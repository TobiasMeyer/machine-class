# Documentation for Machine Building class WS 2019 at FabLab Kamp-Lintfort
[![pipeline status](https://gitlab.com/TobiasMeyer/machine-class/badges/master/pipeline.svg)](https://gitlab.com/TobiasMeyer/machine-class/commits/master)

View this page [here](https://tobiasmeyer.gitlab.io/machine-class/).

## Command to remove metadata from image files
```shell
$ exiftool -all= -overwrite_original -progress ./docs/images/**/*.jpg
$ exiftool -all= -overwrite_original -progress ./docs/images/**/*.png
```
