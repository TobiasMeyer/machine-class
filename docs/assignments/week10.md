# 10. Control Software

## Failing to flash fabricated PCB
The orginal plan was to flash GRBL to the [satshakit-grbl](https://github.com/satshakit/satshakit-grbl) board I fabricated in the previous assignment, but sadly I was unable to do so. The main curlprit I have identified for my issues is that during the soldering of the headers some of the traces broke for pins which were needed for programming.

![Arduino with satshakit-grbl board](../images/week10/arduino-with-satshakit.jpg)

## Arduino with CNC shield
Due to the aforementioned problems I decided to use an [Arduino Uno](https://store.arduino.cc/arduino-uno-rev3) with a [CNC shield](https://blog.protoneer.co.nz/arduino-cnc-shield/) to control the CNC machine.

![Arduino with shield](../images/week10/arduino-with-shield.jpg)

When connecting the whole assembly of Arduino and shield my notebook automatically shutdown the USB ports since it detected the shield using to much power. This issue was easily fixed by taking off the CNC shield.

![Arduino without shield](../images/week10/arduino-without-shield.jpg)

### Uploading Grbl
Having connected the Arduino to my computer it was time to upload [grbg v0.8c](https://github.com/gnea/grbl/wiki) to it. To upload the donwloaded _grbl.hex_ file I used the [paulkaplan/HexUploader](https://github.com/paulkaplan/HexUploader/wiki/Using-HexUploader) software.

![HexUploader](../images/week10/hexuploader.png)

After connecting the Arduino I selected the proper Arduino type, port and Baud rate. To now upload the file I clicked the _Upload a Hex File_-Button and chose the _grbl.hex_ file.

### Ultimate Gcode Sender Platform
While every feature of Grbl is available over the serial connection it is in this case much easier to use a graphical interface to control and monitor the CNC machine. I used [Ultimate Gcode Sender Platform v2.0](https://winder.github.io/ugs_website/) as my GUI of choice. It is made open source under the [GPLv3 license](https://www.gnu.org/licenses/quick-guide-gplv3.en.html) and provides a simple enough interface to not be overly intimidating but also complex enough to actually control the machine.

![Ultimate Gcode Sender Platform in use](../images/week10/ugsplatform.png)

### Generating Gcode
Before I can actually use my CNC machine I have to export the appropiate Gcode for the Grbl controller.
To do this I have to follow the normal steps for the CAM setup until the post-processing steps. In the post processing step you have to choose the _Grbl_ post processor available under _Generic Posts_ and disable the following options:

- Write machine
- Write toollist
- Output M6
- Output tool number

![Post processing options for Grbl](../images/week10/grbl-post-processing-settings.png)
