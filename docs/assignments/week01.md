# 1. Project Management

The first week was spend meeting the other cource participants, discussing the organisation, setting up our website for documentation and me getting a safety introduction.

## Documentation
**If it wasn't documentated, it didn't happen!** was on one of the slides and there are few statements I can agree more with. One of the most frustrating things when working with enterprise code is often not having (adequate and up-to-date) documentation and having to find out everything for yourself or bug the people who wrote the code, if they are even still working at the company.

For that reason it is required to setup and fill this documentation website if you want to pass the course.

### Setup
Initial setup was very simple thanks to a template site provided by our tutor. All the files are stored in a [public repository](https://gitlab.com/TobiasMeyer/machine-class) on [GitLab](https://gitlab.com/). The template included a _.gitlab-ci.yml_ file which is a special file that GitLab can interprete to setup a CI pipeline. This CI pipeline builds our site on every push to the master branch and deploys the site to [GitLab Pages](https://docs.gitlab.com/ee/user/project/pages/).

I extended my pipeline configuration a bit to build the site also on pushes to other branches and publish the generated site as an artifact of the job.
Please notice that the site is only made publically availabe when on the master branch.
This setup allows me to get the same build environment for every branch and download the generated site for testing and preview purposes without having to make it visible for everyone.
You can find the current stable version of my _.gitlab-ci.yml_ [here](https://gitlab.com/TobiasMeyer/machine-class/blob/master/.gitlab-ci.yml).

### Filling the site
Before filling the site some cleanup was needed to remove some unwanted placeholder pages and sample images.

As the site uses the [MkDocs project](https://www.mkdocs.org/), the documentation itself is written in _Markdown_ and you can find an introductory tutorial at [www.markdowntutorial.com](https://www.markdowntutorial.com/).
To edit Markdown files and to develop in general I use the [Atom editor](https://atom.io/) by GitHub due to its very flexible nature and large extension ecosystem. Specifically for Markdown I use the following extensions:
 - [language-markdown](https://github.com/burodepeper/language-markdown) for syntax highlighting and context-aware behaviour
 - [markdown-preview](https://atom.io/packages/markdown-preview) to get a live preview of the generated page inside the editor itself

What this Markdown editing flow usually looks like you can see in this picture:
 ![](../images/week01/markdown-editing-flow.png)

### Pushing the changes
_Making local edits is one thing, but sharing them with the world is a whole other thing._ Thanks to the usage of [Git](https://git-scm.com/) for version control this part is still easier than in many other cases.

While it is fully possible to control everything GIT-related from the command line, in general I find it faster to stay in my editor and do most common things from there. This is possible thanks to the Atom core packages and the [Git-Plus](https://atom.io/packages/git-plus) extension.

## Working with Git
Since I have already used Git a lot prior to this course I had everything installed and setup to work with GitLab, so I will skip that part for now and only mention what needed to be done specific to this course.

### 1. Creating and cloning the repository
For this course I created a public repository on GitLab you can find [here](https://gitlab.com/TobiasMeyer/machine-class). To work with this repository we will first have to clone it locally.

The following command clones the repository into the local directory machine-class via the SSH-URL:
```sh
git clone git@gitlab.com:TobiasMeyer/machine-class.git
```
### 2. Committing changes
After making some changes to your codebase you want to commit these. If you make more changes to files already checked in and make a mistake with Git you have a history of what the file content was before and how it evolved. This allows you to revert your changes and fix your mistakes. Therefore it is best practice to keep commits as small as possible, usually one to two files and only a handful of lines, which allows you and other contributors to see what was done, when and by whom.

The following commands first add the _README.md_ file to the changes to be commited and then commit those changes:
```sh
git add README.md
git commit -m "COMMIT MESSAGE"
```

### 3. Pushing changes
Every few commits and definetly before you go off to do something else you should push your commits to your repository hoster. By doing this you don't lose changes when your computer suddenly dies and other contributors can see what you have done.

The following command pushes your local commits to your repository hoster:
```sh
git push
```

### Custom commands
I have found the _git log_ output to be to verbose to get a quick overview, which is why I mostly use a custom command called via _git grog_. The _grog_ part stands for _**gr**aphical l**og**_ and my version is a slight derivation of the one mentioned in the article [Lesser Known Git Commands](https://dzone.com/articles/lesser-known-git-commands) by [DZone](https://dzone.com/).

![](../images/week01/git-grog-output.png)

You can get it for yourself by running:
```
git config --global alias.grog 'log --graph --abbrev-commit --decorate --all --format=format:"%C(bold blue)%h%C(reset) - %C(bold cyan)%aD%C(dim white) - %an%C(reset) %C(bold green)(%ar)%C(reset)%C(bold yellow)%d%C(reset)%n %C(white)%s%C(reset)"'
```
