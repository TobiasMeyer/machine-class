# 6. 3D Milling

## Holder for Proxxon MICROMOT 50/EF
The part of the final project I decided to 3D mill is a holder for a _Proxxon MICROMOT 50/EF_, which was already described in _Assignment 2_.
Only the size of the hole for the set screw has been decreased to have the right diameter for cutting out the thread of an M5 screw.

[Download .f3d](../../files/week06/Proxxon%20Holder%20v9.f3z)

![Holder with Proxxon](../images/week06/holder-with-proxxon.png)

## Stock Selection
Before selecting a piece of stock you have to know the overall dimensions of the model you are trying to mill out.
For my model the dimensions are **55mm x 45mm x 25mm**, which I measured by using the _Inspection_ tool in _Fusion 360_.


For stock selection it is important to choose the most appropiate material for the application and select a piece that is not overly large as to not waste more material than neccessary but still give some space on the sides to mill away and get good surfaces.

To select the stock I looked for pieces with roughly the right size and then used calipers to find the best one.
The chosen stock piece pictured below has the following dimensions: **69.88mm x 50.36mm x 32.29mm**
![Chosen Stock](../images/week06/stock-dimensions.jpg)

As you can see the model perfectly fits inside the piece with enough material left on the sides.
Please notice that the model is placed at a zero millimeter offset from the bottom.
![Model inside Stock](../images/week06/model-in-stock.png)

## CAM Setup
For the CNC machine to know what to do the toolpaths have to be designed in a CAM software.
I used Fusion 360 to do this.

The milling has been divided up into four steps to go from raw stock to finished piece.
This is done since different parts of the piece need different surface finishes and a tool change was required to achieve this.

### 1. Getting the right height
In the first pass just the material above the top edge of the model was removed using _Adaptive Clearing_ with a _6mm Flat end mill_.

![1. Removing top layer toolpath](../images/week06/1_removing-top-layer_toolpath.png)
![1. Removing top layer settings](../images/week06/1_removing-top-layer_settings.png)

### 2. Roughing out the center hole
To remove most of the material from the center hole a _Pocket Clearing_ pass with a _6mm Flat end mill_ was used.

![2. Roughing out center hole](../images/week06/2_roughing-hole_toolpath.png)
![2. Roughing out center hole settings](../images/week06/2_roughing-hole_settings.png)


### 3. Cleaning up the center hole
To remove the rest of the material left in the previous pass ang get a good surface finish a _Contour_ pass with a _6mm Ball end mill_ was used.

![3. Cleaning up center hole](../images/week06/3_cleaning-up-hole_toolpath.png)
![3. Cleaning up center hole settings](../images/week06/3_cleaning-up-hole_settings.png)

### 4. Cutting away the outside
To remove the material left standing around the outside and get the correct overall dimensions a _2D Adaptive Clearing pass with multiple depths_ and a _6mm Flat end mill_ was used.

I tried to use a _3D Adaptive Clearing_ pass, but I could not get it to only cut the outside even after fiddling around with all the available settings.

![4. Cutting away the outside](../images/week06/4_outside_toolpath.png)
![4. Cutting away the outside settings](../images/week06/4_outside_settings.png)

### Download
The above four passes were post-processed and combined into three runs based on the tool used.

 - [_1. Getting the right height_ and _2. Roughing out the center hole_](../../files/week06/toolpath_1-and-2.cnc)
 - [_3. Cleaning up the center hole_](../../files/week06/toolpath_3.cnc)
 - [_4. Cutting away the outside_](../../files/week06/toolpath_4.cnc)

## Reality
As with everything a plan is a nice thing but in reality not everything will work as expected.

### 0. Preparing the selected stock
The selected piece of stock was fixated to the wooden plate used as a sacrifial layer using double-sided tape.
In addition the vacuuming abilities of the CNC machine were used.

![Stock Preparation](../images/week06/reality_0.jpg)

### 1. Getting the right height _and_ 2. Roughing out the center hole
Like explained above the task of the first job was to remove the stock material avove the model itself and rough out the hole.

This job worked without any problems.

![1. Getting the right height and 2. Roughing out the center hole](../images/week06/reality_1-2.jpg)

### 3. Cleaning up the center hole
After the center hole has already been roughed out in the previous job, in this job it was cleaned up using a 6mm ball end mill.

Sadly this job was not entirely without problems.
Since this job needs a different tool than the previous job it had to be changed. During the tool changing process I managed to trigger a bug in the tool changer and crash the machine.
Due to a quick press of the emergency off switch neither the machine nor any material was damaged with the only implication beeing that the previous zero position was no longer 100 percent on point.
This resulted in this job and the next one not beeing executed perfectly.

Happily for me this part does not need a hundred percent of accuracy as a set screw is used to hold the Proxxon in place.

![3. Cleaning up the center hole](../images/week06/reality_3.jpg)

### 4. Cutting away the outside
At last the tool had to be changed again and the rest of the outside finished.

This job worked without any problems apart from the aforementionend ones above, which you can see in the wood beeing barely cut into instead of the specified 0.3 millimeters due to the Z axis beeing slightly offset.

![4. Cutting away the outside](../images/week06/reality_4.jpg)
