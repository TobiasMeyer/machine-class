# 9. Machine electronics
The goal of this weeks assignment is to fabricate an existing PCB design from generating the toolpaths to machining and soldering the PCB.

## satshakit-grbl
The existing PCB design I am copying in this assignment is the [satshakit-grbl](https://github.com/satshakit/satshakit-grbl) board, which is a small microcontroller board designed to work with [Grbl](https://github.com/gnea/grbl/wiki). Grbl is a software designed to control and monitor machines like 3D printers, CNC machines and laser cutters and is open sourced under the [GPLv3 license](https://www.gnu.org/licenses/quick-guide-gplv3.en.html).

## 1. Milling out the PCB
Starting with a raw PCB sheet it was time to use a Roland Modela MDX-40A CNC mill to cut the traces into it and the board out of the sheet itself.

![Roland Modela MDX-40A CNC mill](../images/week09/roland.png)

Here you can see the board with finished traces being cut out of the sheet:
![Cutting out the board](../images/week09/cutting-out-near.jpg)

Resulting in a blank PCB:
![Blank PCB](../images/week09/blank-pcb.jpg)

## 2. Soldering
After cleaning up the blank PCB I printed out the bill of materials for the board, put a strip of double-sided tape next to the list and got all the components from the cupboard. This way I got a nice organised sheet with all the parts needed for assembly, which is especially important since SMD parts are very small and not all are labeled.

![Organised components](../images/week09/organised-components.jpg)

With all the needed parts prepared I got to work soldering the components onto the board.

![Assembled board](../images/week09/assembled-board.jpg)

### Problems
While I learned a lot during this assignment it was one of the most frustrating ones for me. Since the board has a relatively complex design with thraces beeing close to eachother it is very important to use the right milling settings, which resulted in me needing multiple tries to get board shown above which still isn't perfect as it is missing the traces for the USB port. In addition one perfectly fine board was destroyed when during the machining of the outline of the board the PCB sheet shifted and the tool was driven directly though one side of it.

As anyone familiar with soldering will have noticed many of the soldered points aren't perfect. This was my second time actually soldering a board with about three years between this one and the previous one which also used bigger through hole components. Overall this means that while my skills have significantly improved during and after soldering this board, this board is non functional due to some traces breaking off when soldering the headers on.
