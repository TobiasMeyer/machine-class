# 3. Laser Cutting
This week we got an introduction to using _Laser Cutters_ and learned to design pressfit structures.
To demonstrate we were tasked with creating our own pressfit structure, which should also be used for our final machine project.

## Frame Support Beam
Since I intend to use a T-shape for my final machine the profile forming the I-part has to be supported from the sides to withstand the load from milling. One of these support beams is supposed to go on either side of the profile.

### Original Design
The original design was a solid 3D model to be fabricated with a 3D Printer or CNC machine.
For this weeks assignment I turned the original model into one that could be frabricated with a laser cutting with the parts being pressfit together.

[Download](../../files/week03/model-original.f3d)

![Solid 3D Part](../images/week03/original.png)

### Version 0
_Version 0_ of the pressfit part consists of two side panels with holes and cutouts for a center piece and two edge pieces.
Before laser cutting this part I made several testcuts of the connecting holes to find the right laser settings and found that the fit was too loose and the width of the parts going into the holes had to be increased by about 0.3 to 0.4 millimeters.

![Assembled Part](../images/week03/frame-support-beam.png)

#### Laser Cutter Settings
During the testing mentioned above we found the following settings to be optimal to cut our acrylic material.
They were then also used for the following cuts.

![Laser Cutter Settings](../images/week03/laser-cutter-settings.png)

Some of the testcuts at different settings:
![Connection Testcuts](../images/week03/connection-testcuts.jpg)

### Version 1
_Version 1_ is just a variation of _Version 0_ with the width of the parts going into the cutouts increased by 0.3mm to get a thighter fit.

[Download](../../files/week03/Model%20v1.f3d)

I was able to cut out all the parts and assemble the beam...
![Version 1 assembled](../images/week03/v1-assembled.jpg)

... but sadly the distance between the cutout and the edge of tha part was too small and a corner broke off.
![Version 1 with broken corner](../images/week03/v1-broken-corner.jpg)

And yet there is hope because even in its broken state the assembled part was able to support another students full weight of about 80kg.
I would definetly consider this a success!
![Standing on it](../images/week03/v1-standing-on-it.jpg)

### Following versions
For following versions I want to apply what I have learned from the problems of the above versions.

Those in particular would be:

 + Increase the width of the part going in to 0.4mm above the width of the hole to get better tightness
 + Work with multiple passes to first cut out any holes and only then the part itself
 + Leave more space between the cutouts and the edges of the part
 + Maybe use screws to hold it together in the middle section
