# 2. CAD Design
This week it was time to think about what type of machine I want to build for my final course project. After some time thinking about it I landed on a small CNC machine for cutting softer materials like foam and wax. This focus on softer materials should allow me to keep the machine small and relatively cheap.

## Working with existing models
Not every part is going to be unique and many parts can be bought directly off the self. Therefore for many of these parts someone has already created a CAD model and shared it somewhere.

### How to import them
 1. Open up **Fusion 360**
 2. Open or create a design
 3. Choose the menu item **File** > **Upload**
   1. Choose the model file
   2. Select the folder to upload into
   3. Click on **Upload** button
   4. Wait for upload to finish
 4. Navigate in **Data Panel** to your chosen folder
 5. Right-click on the uploaded model and select the option **Insert into Current Design**

 ![Example of inserting model](../images/week02/insert-into-current-design.png)

### Proxxon MICROMOT 50/EF
The actual milling my CNC will be doing is done by a **Proxxon MICROMOT 50/EF** and I found a model of it [here](https://grabcad.com/library/proxxon-micromot-50-ef-1) on GrabCad.
![Proxxon MICROMOT 50/EF](../images/week02/Proxxon-MICROMOT-50EF.png)

## Inspriation
Since I am not the first to build a CNC machine I was able to take inspiration from the machines already available in the FabLab and work by others.

Listed below are some YouTube videos I was inspired by:

 + [Banggood 3018 Mini Cnc Router Kit Build, Test & Review](https://www.youtube.com/watch?v=hB4Cvbtu-lA) by [techydiy](https://www.youtube.com/channel/UCtMM9yct8wrdLEZNwLkj-2Q)
 + [CNC | Complete Assembly and First Print!](https://www.youtube.com/watch?v=XfhlZuw5mDs) by [3DSage](https://www.youtube.com/channel/UCjdHbo8_vh3rxQ-875XGkvw)
 + [DIY Cheap Arduino CNC Machine - Machine is Complete AND Accurate!](https://www.youtube.com/watch?v=WKmzlMvxC7I) by [NYC CNC](https://www.youtube.com/channel/UCe0IyK4ntgdPTTjsxjvyHPg)

## My machine
As mentioned in the intro my machine is a CNC machine focused on cutting softer materials like foam and wax.

### Holder for the Proxxon MICROMOT 50/EF
The _Proxxon MICROMOT 50/EF_ has no built-in fixation points and needs a holder. The design I landed on uses a hole following the contours of the device combined with a screw to fixate it. It also has two screw holes to fixate the holder itself to some kind of backplate.

[Download](../../files/week02/Holder%20for%20the%20Proxxon%20MICROMOT%2050EF.f3z)

![Overview](../images/week02/holder-micromot.png)

#### Getting the contours right
Since my part uses a friction fit combined with a srew it had to have the exact outline of the Micromot.
To achieve this feature I started off with a box, sized in a way to allow space for the hole and leave enough material for the holes, placed the imported model of the _Proxxon MICROMOT 50/EF_ where I wanted it inside the block and used a tool from the **Solid** tab found under **Modify** > **Combine** and used it with the **Cut** operation, where the box was used as the _Target Body_ and the Micromot as the _Tool Bodies_.

In the end this resulted in the following cross-section:
![Contour cross-section](../images/week02/holder-micromot-contures.png)

#### Sunken screw holes
As it is not needed to have screws going through the whole part to fixate it to the backplate, I used sunken screw holes to decrese the distance of the screw inside the model.
![Sunken screw holes](../images/week02/holder-micromot-screw-holes.png)

### Linear Bearing Holder
In my machine I plan on using rods for linear guidance and while there are already linear bearing holders available I decided to create my own to be able to control its exact dimensions.

[Download](../../files/week02/Linear%20Bearing%20Holder.f3d)

![Linear Bearing Holder](../images/week02/linear-bearing-holder.png)

#### Constraining the sketch
In **Fusion 360** in addition to just drawing shapes you can use constraints to control exact dimensions and behaviours.

In the sketch below you can see that I made use of **Sketch Dimension** constraints, but also of the **Midpoint** constraint to keep the bearing hole always located in the midpoint of the part.
![Sketch](../images/week02/linear-bearing-holder-sketch.png)

#### Drawing
This is a part that could by mass-manufactured and below you find the drawing that any factory around the world could use to produce the part.

[Download](../../files/week02/Linear%20Bearing%20Holder%20Drawing.pdf)

![Linear Bearing Holder Drawing](../images/week02/linear-bearing-holder-drawing.png)
