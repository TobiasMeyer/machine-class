# 11. Final project

## Design and Production
The goal of my design is to provide an inexpensive but sturdy and usable machine, which uses off-the-shelve parts wherever possible to keep costs and machining time manageable.

My final design ended up being a three axis CNC machine with a fixed gantry and a theoretical working area of 280x280x75mm.

![Overview of complete machine](../images/week11/overview.png)

Below you can download the _Fusion 360_ file for the whole machine. For manufacturing purposes the custom components of the machine have been saved in invdiviual files and inside them you will find the settings used to manufacture each part. The invdiviual parts are provided for downloading below in the appropriate sections. During the production of some parts some settings have been adjusted directly on the machine and are not reflected in the settings. When trying to replicate these parts please keep this in consideration and adjust as needed.

[Download .f3z](../../files/week11/Machine%20v68.f3z)

### X-Axis
The X-Axis provides the base of the machine and has to be very sturdy. The shoulders of the Y-Axis are directly attached to its sides.

![X-Axis overview](../images/week11/x-axis/overview.png)

#### Frame
The _Frame_ is the main part that is giving the X-Axis its strength. It is made up of six 30x30mm Aluminium profiles which are held together by angle brackets. The four center profiles have a length of 50cm and the two profiles on the outside have a length of 40cm.

![Frame overview](../images/week11/x-axis/frame/overview.png)

#### Motor Holder
The _Motor Holder_ is attached to the motor driving the lead screw for the X-Axis on the back and to the two innermost profiles on the sides. This design both provides the right distance between the two profiles for the rails to be aligned to the holes in the _Reference Plate_ while also centering the motor.

[Download .f3d](../../files/week11/x-axis/Motor%20Holder%20v4.f3d)

![Motor Holder overview](../images/week11/x-axis/motor-holder/overview-design.png)

#### Bed Assembly
![Bed Assembly overview](../images/week11/x-axis/bed-assembly/overview-design.png)
##### Reference Plate
[Download .f3d](../../files/week11/x-axis/bed-assembly/Reference%20Plate%20v4.f3d)

![Reference Plate overview](../images/week11/x-axis/bed-assembly/reference-plate/overview-design.png)

##### Guide Block Spacer
![Guide Block Spacer overview](../images/week11/x-axis/bed-assembly/guide-block-spacer/overview-design.png)

##### Lead Screw Receiver
[Download .f3z](../../files/week11/x-axis/bed-assembly/Lead%20Screw%20Receiver%20v4.f3z)

![Lead Screw Receiver overview](../images/week11/x-axis/bed-assembly/lead-screw-receiver/overview-design.png)
#### Endstop Target
The _Endstop Target_ is intended to be used as a target for an endstop located on the underside of the reference plate. This part is necessary since the the only point where the reference plate comes close to another usable target point is the motor holder between the two innermost profiles. The problem with positioning the endstop there would be that there is no good was to route the cable from the endstop to the controller. In addition the exact position of the _Endstop Target_ is adjustable.

Download
[.f3d](../../files/week11/x-axis/Endstop%20Target%20v1.f3d),
[.stl](../../files/week11/x-axis/Endstop%20Target%20v1.stl)

![Lead Screw Receiver overview](../images/week11/x-axis/endstop-target/overview-design.png)

### Y-Axis
The _Y-Axis_ uses fixed sides with two 42cm profiles connecting them. On the profiles 40cm linear guide rails are mounted where the _Z-Axis_ can move.

![Y-Axis overview](../images/week11/y-axis/overview-design.png)

#### Left Shoulder
In the _Left Shoulder_ two sunken holes for the Aluminium profiles, a cutout for the coupler and leadscrew and screw to mount the motor on the outside are integrated. Integrating sunkel holes for all the motor screws creates a flat surface allowing the _Z-Axis_ to reach its full movement potential.

[Download .f3z](../../files/week11/y-axis/Left%20Shoulder%20v5.f3z)

![Left Shoulder overview](../images/week11/y-axis/left-shoulder/overview-design.png)

#### Right Shoulder
The _Right Shoulder_ is identical to the _Left Shoulder_ except for the missing motor mounting point.

[Download .f3d](../../files/week11/y-axis/Right%20Shoulder%20v7.f3d)

![Left Shoulder overview](../images/week11/y-axis/right-shoulder/overview-design.png)

### Z-Axis
The _Z-Axis_ is the most complex assembly in the whole machine as it was optimized to have as small a width as possible to make the most of the space available for movement.

![Z-Axis overview](../images/week11/z-axis/overview-design.png)

#### Top Plate
The _Top Plate_ hase holes for the motor mount, the guide rods and the Aluminium profiles located on the sides.

[Download .f3z](../../files/week11/z-axis/Top%20Plate%20v5.f3z)

![Top Plate overview](../images/week11/z-axis/top-plate/overview-design.png)

#### Bottom Plate
The _Bottom Plate_ is a simplified and streamlined version of the _Top Plate_. This is possible since it doesn't need holes for the motor.

[Download .f3d](../../files/week11/z-axis/Bottom%20Plate%20v5.f3d)

![Bottom Plate overview](../images/week11/z-axis/bottom-plate/overview-design.png)

#### Motor Spacer
The _Motor Spacer_ is the main component allowing the whole _Z-Axis_ assembly to be much thinner. The used motor has a circular protusion in the middel. The cutout for this protrusion would otherwise have to be integrated into the _Top Plate_, which thanks to the machining direction would required the hole to go the whole way through and would mean the top plate would be much wider to allow for it.

![Motor Spacer overview](../images/week11/z-axis/motor-spacer/overview-design.png)

#### Bed Assembly
This assembly connects the moving Proxxon to the static _Z-Axis_ frame.

![Motor Spacer overview](../images/week11/z-axis/bed-assembly/overview-design.png)

##### Holder
This part provides attachment points for the actual spindle holder.

[Download .f3d](../../files/week11/z-axis/bed-assembly/Holder%20v11.f3d)

![Holder overview](../images/week11/z-axis/bed-assembly/holder/overview-design.png)

##### Proxxon Holder
The _Proxxon Holder_ has a hole to fit a _Proxxon MICROMOT 50/EF_ inside and is the only part that has to be replaced if there is a desire to use another spindle.

For more on this part please take a look at [6. 3D Milling](../week06/).

[Download .f3z](../../files/week11/z-axis/bed-assembly/Proxxon%20Holder%20v1.f3z)

![Proxxon Holder overview](../images/week11/z-axis/bed-assembly/proxxon-holder/overview-design.png)

#### Y-Axis Mount
As described before the _Y-Axis_ has two linear rails mounted on its connecting profiles. This assembly is designed to fit onto it and provide the attachment for the rest of the _Z-Axis_ to the overall machine.

![Y-Axis Mount overview](../images/week11/z-axis/y-axis-mount/overview-design.png)

##### Mount Plate
Mounting plate to attach to _Lead Screw Receiver_ and linear guide blocks on one side and to the profiles of the _Z-Axis_ on the other.

[Download .f3d](../../files/week11/z-axis/y-axis-mount/Mount%20Plate%20v5.f3d)

![Mount Plate overview](../images/week11/z-axis/y-axis-mount/mount-plate/overview-design.png)

##### Lead Screw Receiver
Designed to fit an antibacklash nut and align with the lead screw coming from the side of the _Y-Axis_.

[Download .f3z](../../files/week11/z-axis/y-axis-mount/Lead%20Screw%20Receiver%20v8.f3z)

![Mount Plate overview](../images/week11/z-axis/y-axis-mount/lead-screw-receiver/overview-design.png)

## Assembling everything
Having manufactured all parts I started assembling everything together.

### Y-Axis
As the [Y-Axis](#y-axis) is the simplest axis to assemble and its width is needed to assemble the [X-Axis](#x-axis) I decided to start with it.

![Assembled Y-Axis](../images/week11/assembly/y-axis.jpg)

Everything went together as planned.
Weirdly while the profiles fit into the [Right Shoulder](#right-shoulder) I had some difficulties getting them to fit into the [Left Shoulder](#left-shoulder) and had to manually enlargen the holes in that side.

Overall this axis turned out well and feels very sturdy when being picked up.

### Z-Axis
Next up I assembled the [Z-Axis](#z-axis).
One aspect that I didn't pay attention to was that the profiles can rotate freely while being tightened down.
To alleviate this problem I clamped down the profile to the table.

![Z-Axis clamped down](../images/week11/assembly/z-axis-clamped-down.jpg)

After securing the [Bottom Plate](#bottom-plate) I hammered in the guide rods and slid the [Holder](#holder) in.
Finishing everything by installing the [Top Plate](#top-plate), the motor spacer and the motor with attached lead screw.

![Z-Axis assembled (View from backside)](../images/week11/assembly/z-axis-assembled-back.jpg)

### X-Axis
With both other axes assembled the only thing left to do was to assemble the [X-Axis](#x-axis).
Using angle brackets to get the profiles perfectly perpendicular to each other I am going corner by corner to assemble the outer profiles.
Before screwing done the second side profile the already assembled [Y-Axis](#y-axis) was attached to the other side to get the exact distance between the two profiles right.

![Squaring X-Axis frame](../images/week11/assembly/squaring-x-axis-frame.jpg)

After assembling the outer frame the two innermost profiles, already attached to the [Reference Plate](#reference-plate) and the [Motor Holder](#motor-holder), was put in and aligned with the center using a caliper.
The beautiful result of all this you can see below.

![Squaring X-Axis frame](../images/week11/assembly/squared-x-axis.jpg)

Completing the above steps we end up with a nearly finished machine.

![Nearly finished machine](../images/week11/assembly/nearly-finished-machine.jpg)

### Final touches
After attaching the [Proxxon Holder](#proxxon-holder) and the Arduino with CNC shield I connected everything to my MacBook and fired up [Ultimate Gcode Sender Platform v2.0](https://winder.github.io/ugs_website/).

![Fully assembled machine](../images/week11/assembly/fully-assembled.jpg)

## Grbl configuration
While the default settings for _Grbl_ are chosen to get the machine moving safely without any changes they are not accurate in terms of how many steps are actually needed to move an axis a certain amount of millimeters.
In the following I will shortly describe which settings I changed and provide the full settings list of my machine. These settings are not final and only meant to be good enough to get a first testcut done.

If you want more info about the used electronics have a look at [10. Control Software](../week10/) where I go into more detail.

### X-Axis
While the X-Axis moves very nicely for the most part when it comes closer to the front it starts loosing some steps due to a slight misalignment between the height of the lead screw receiver and the center of the motor.
For this reason I selected the furthstest most point as the zero point of the X-Axis.
With its configuration to use quartersteps and a T8\*2\*8 leadscrew it has **100 steps/mm** and **200mm of movement range**.
In testing it had some problems with fast movements and was run at **100 mm/min**, but it should be able to be run at much higher speeds once the misalignment is fixed.

### Y-Axis
The Y-Axis has the same problem with the misalignment of the lead screw receiver and the motor center but it is more noticeable on this axis.
As it has the same configuration as the X-Axis it also uses **100 steps/mm** and ran at **100 mm/min**, but has a maximum **movement range of 230mm**.

### Z-Axis
The Z-Axis is the best moving axis out of all three and is configured to use **sixteenth steps**.
In combination with a T8\*2\*8 leadscrew this results in **400 steps/mm** and **75mm of movement range**.
In testing moving at a rate of **500 mm/min** worked reliably with higher speeds also not seeming to be a problem.

<video src="../../files/week11/z-axis-moving.mp4" width="400" autoplay loop></video>

### Final configuration
In the end I landed on the following Grbl configuration, which is definetly not yet perfectly optimized but is good enough to do some first cuts and has the appropriate values for the steps/mm for the axes and good softlimits.
```
$0 = 10    (Step pulse time, microseconds)
$1 = 25    (Step idle delay, milliseconds)
$2 = 0    (Step pulse invert, mask)
$3 = 4    (Step direction invert, mask)
$4 = 0    (Invert step enable pin, boolean)
$5 = 0    (Invert limit pins, boolean)
$6 = 0    (Invert probe pin, boolean)
$10 = 1    (Status report options, mask)
$11 = 0.010    (Junction deviation, millimeters)
$12 = 0.002    (Arc tolerance, millimeters)
$13 = 0    (Report in inches, boolean)
$20 = 0    (Soft limits enable, boolean)
$21 = 0    (Hard limits enable, boolean)
$22 = 0    (Homing cycle enable, boolean)
$23 = 0    (Homing direction invert, mask)
$24 = 25.000    (Homing locate feed rate, mm/min)
$25 = 500.000    (Homing search seek rate, mm/min)
$26 = 250    (Homing switch debounce delay, milliseconds)
$27 = 1.000    (Homing switch pull-off distance, millimeters)
$30 = 1000    (Maximum spindle speed, RPM)
$31 = 0    (Minimum spindle speed, RPM)
$32 = 0    (Laser-mode enable, boolean)
$100 = 100.000    (X-axis travel resolution, step/mm)
$101 = 100.000    (Y-axis travel resolution, step/mm)
$102 = 400.000    (Z-axis travel resolution, step/mm)
$110 = 500.000    (X-axis maximum rate, mm/min)
$111 = 500.000    (Y-axis maximum rate, mm/min)
$112 = 500.000    (Z-axis maximum rate, mm/min)
$120 = 10.000    (X-axis acceleration, mm/sec^2)
$121 = 10.000    (Y-axis acceleration, mm/sec^2)
$122 = 10.000    (Z-axis acceleration, mm/sec^2)
$130 = 200.000    (X-axis maximum travel, millimeters)
$131 = 230.000    (Y-axis maximum travel, millimeters)
$132 = 75.000    (Z-axis maximum travel, millimeters)
```

## First test cut
After testing the axis seperately it is time to test the working of the whole system.
To do this I measured a block of foam, modeled a simple _T_ for Tobias in Fusion 360 and exported the Gcode as described in the previous assignment.

[Dowload .f3d](../../files/week11/Test%20T%20v8.f3d)

![Test T in Fusion 360](../images/week11/test-t-fusion.png)

With the block of foam attached to the bed...
![Attached block of foam](../images/week11/test-t-foam-attached.jpg)

and the generated Gcode loaded into [Ultimate Gcode Sender Platform v2.0](https://winder.github.io/ugs_website/)...
![Test T loaded in Ultimate Gcode Sender Platform](../images/week11/ugsplatform.png)

I set the zero point and started the machine...

<video src="../../files/week11/first-cut.mp4" width="600" controls poster="../../images/week11/test-t-result.jpg"></video>

and got the following result.
![Test T result](../images/week11/test-t-result.jpg)

As you can see the program ran to completion and while it missed some steps in the Y-Axis due to the the motor driver overheating the results in the other axes look great. I am very happy with this result especially for the first part manufactured on my machine.
