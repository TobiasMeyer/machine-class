# 5. 2D Milling
Make a pressift/bolted structure using 2D Milling that you cannot break with your hands and can stand on.

## Design of Phone Holder
Since I found no part that can be 2D milled and used for my final project I decided to make something useful for myself. The part I landed on is a holder for my phone.

### Google Pixel 2 XL
Before I was able to start modeling the phone stand I had to model the phone itself. For this I measured my phone and used those measurements to constrain a sketch in Fusion 360, which was then extroduded to the appropiate thickness. Since my case is not a rectangle I also modeled the rounded corners and the chamfered edges.

![Google Pixel 2 XL](../images/week05/google-pixel-2-xl.png)

### Phone stand
Based on the model of my smartphone I started designing the stand itself.

[Download .f3d](../../files/week05/Phone%20Holder%20v8.f3d)

Since all parts have to fit together I decided to start with modeling the bottom plate as it dictates the measurements for all other parts.
It is designed to not be too large for the phone but still leave enough clearance to minimize the risk of scrachting it.
![Bottom Plate fitment](../images/week05/bottom-plate-fitment.png)

After some trial and error getting the sizing right for the bottom plate and using the modeled phone as reference, I designed the other plates resulting in the assembled structure below.
![Phone Holder assembled](../images/week05/phone-holder-assembled.png)

## Preparing for 2D Milling
I designed the parts as 3D models in Fusion 360 for easier visualization and therefore faster design iteration, but to be able to 2D mill these parts I first had to convert the outlines to 2D sketches.

In _Fusion 360_ this can easily be done by creating a new sketch and projecting the outline of a part onto the sketch.
Below you can see this process applied to one of the side plates.
![Side Plate projected onto sketch](../images/week05/projected-side-plate.png)

This process was repeated for all the parts of the stand and the resulting sketches were exported as _.dxf_ files.
All the different _.dxf_ files were then opened in the CAD software [Rhinoceros](https://www.rhino3d.com/de/) and combined into one single file, which in theory would allow all parts to be cut in a single machining job.
![Combined .dxf files](../images/week05/combined-dxf-files.png)

Please note the red outline of the inner holes.
This difference in coloring means that those cuts are on another layer, which is needed since the machine needs to cut inside the lines and it is advisable to cut out the holes before the actual part to minimize the risk of the part coming loose.

You can also see some holes in the layout, which are called _dog bones_ and are needed because the end mill used to cut the parts is round and cannot cut perfectly straight corners and therefore those holes have to be added where two parts have to touch in the corners.

### Download
You can download the single part layouts...

 - [bottom-plate.dxf](../../files/week05/bottom-plate.dxf)
 - [back-plate.dxf](../../files/week05/back-plate.dxf)
 - [front-plate.dxf](../../files/week05/front-plate.dxf)
 - [side-plate.dxf](../../files/week05/side-plate.dxf)

or the combined design [combined.dxf](../../files/week05/combined.dxf).

## 2D Milling
After all the modeling and preparation it came the time to get down to work and start cutting some Aluminium.

![CNC in action](../images/week05/machine-in-action.jpg)

As mentioned above all the _.dxf_ files were combined into one design to minimize material waste and to cut out all parts in a single machining run, but sadly the adhesion of the Aluminium plate to the bed was not strong enough and the parts all came of during the last layer, which meant the parts would still come out great but the job had to be restarted after every single part.

## Assembled product
All the parts came out great and after removing all the metal chips and the tape I used a belt sander to knock off any sharp edges.
The parts fit together without any problems and the end result can speak for itself.

![Real assembled phone stand](../images/week05/real-assembled-phone-holder.jpg)
