# 4. 3D Printing
This week we were introduced to 3D Printing and were tasked with printing a part of our own design. The part intention should be to connect two mechanical parts.

## Support Beam
The previous week I made a support beam made out of laser cut parts which were pressfitted together. While it worked the original design was intended to be 3D printed or milled out and this is what I am planning to do this week.

[Download .f3d](../../files/week04/Support%20Beam%20v7.f3d)

![Support Beam V4](../images/week04/support-beam-v4.png)

## Printing it
Before printing it I used the _Ultimaker Cura_ software to prepare the part and slice it using the settings below. The sliced part was then printed on a _Ultimaker S5_ using _Ultimaker Tough PLA 0.4mm diameter_.

### Settings
 - Ultimaker S5
 - Tough PLA
 - Layer height: 0.25mm
 - Wall thickness: 5 lines
 - Infill 50% with grid pattern

[Download .curaprofile](../../files/week04/Strong%20Parts%20profile.curaprofile)

### Testing hole fitment
Thankfully Daniele adviced me to test the fitment of the M8 screws in the holes. This is needed because the PLA shrinks more near holes.

![Hole 8mm](../images/week04/hole-8mm.jpg)

Results (Design vs. Printed Part):

 -  8mm => 7.25mm
   -  Too small for screw
   -  Downloads: [.stl](../../files/week04/hole-8mm.stl), [.upf](../../files/week04/UMS5_hole-8mm.ufp)
 -  9mm => 8.25mm
   -  Screw fits but is a bit on the looser side
   -  Downloads: [ .stl](../../files/week04/hole-9mm.stl), [.upf](../../files/week04/UMS5_hole-9mm.ufp)

In conclusion the PLA shrinks by about 0.75mm in both tries.

### The actual part
With the screw hole size now updated to 9mm I tried printing the beam itself using the settings above.

The print came out great and thanks to the increased hole diameter I was able to fit my screws without problems, as you can see below.

![Printed Beam](../images/week04/printed-beam.jpg)

Downloads:
[.stl](../../files/week04/beam.stl), [.upf](../../files/week04/UMS5_beam.ufp)
