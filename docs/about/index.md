# Hi, I am Tobias!

![](../images/avatar-photo.jpg)

Hello, my name is **Tobias Meyer** and I am currently studying _Medien- und Kommunikationsinformatik_ (or short _MuKi_) at the _Rhine-Waal University of Applied Sciences_.

While my main interest and experience lies in building the backend of complex applications, this site documents my forays into working with laser cutters, 3D printers and designing my own models.

Feel free to check out the rest of my site!
